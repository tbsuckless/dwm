/* See LICENSE file for copyright and license details. */
#ifndef CONFIG_H
#define CONFIG_H

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 6;        /* gap pixel */
static const unsigned int snap      = 16;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "SpaceMono Nerd Font:size=14:antialias=true" };
static const char dmenufont[]       = "SpaceMono Nerd Font:size=14:antialias=true";
static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#329932";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char darkred[]         = "#991010";
static const char red[]             = "#ef2020";
static const char green[]           = "#92d4af";
static const char blue[]            = "#869fb3";
static const char magenta[]         = "#c26cd5";
static const char yellow[]          = "#fff293";
static const unsigned int baralpha  = 0xa0;
static const unsigned int borderalpha = OPAQUE;

static const char *colors[][8]      = {
	/*               fg         bg         border   */
	[SchemeNorm]    = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]     = { col_gray4, darkred, darkred },
	[SchemeOcc]     = { col_gray1, blue, blue },
	[SchemeGreen]   = { green, col_gray1, col_gray1 },
	[SchemeBlue]    = { blue, col_gray1, col_gray1 },
	[SchemeMagenta] = { magenta, col_gray1, col_gray1 },
	[SchemeYellow]  = { yellow, col_gray1, col_gray1 },
	[SchemeRed]     = { red, col_gray1, col_gray1 }
};
/*                   fg       bg        border */
static const unsigned int alphas[][8] = {
	[SchemeNorm]    = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]     = { OPAQUE, baralpha, borderalpha },
	[SchemeOcc]     = { baralpha, baralpha, borderalpha },
	[SchemeGreen]   = { OPAQUE, baralpha, borderalpha },
	[SchemeBlue]    = { OPAQUE, baralpha, borderalpha },
	[SchemeMagenta] = { OPAQUE, baralpha, borderalpha },
	[SchemeYellow]  = { OPAQUE, baralpha, borderalpha },
	[SchemeRed]     = { OPAQUE, baralpha, borderalpha }
};

/* tagging */
static const char *tags[] = { "", "爵", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       1 << 4,       1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
	{ "qutebrowser", NULL,    NULL,       2,            0,           -1 },
	{ "Emacs",    NULL,       NULL,       1 << 3,       0,           -1 },
	{ "emacs",    NULL,       NULL,       1 << 3,       0,           -1 },
	{ "obs",      NULL,       NULL,       1 << 4,       0,           -1 },
	{ "mpv",      NULL,       NULL,       1 << 5,       1,           -1 },
	{ "vlc",      NULL,       NULL,       1 << 5,       1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run_history", "-m", dmenumon, "-w", "780", "-l", "7", "-y", "200", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *qutebrowser[] = { "qutebrowser", "--qt-arg", "stylesheet", "/home/babkock/.local/share/qutebrowser/fix-tooltips.qss", "file:///home/babkock/.config/qutebrowser/index.html", NULL };
static const char *ncmpcpp[] = { "st", "-e", "ncmpcpp", NULL };
static const char *emacs[] = { "emacsclient", "-c", "-a", "'emacs'", NULL };
static const char *mpv[] = { "mpv", "av://v4l2:/dev/video0", "--vo=gpu", "--hwdec=vaapi", "--untimed", "--profile=low-latency", "--no-osc", NULL };
static const char *obs[] = { "obs", NULL };
static const char *pavucontrol[] = { "pavucontrol", NULL };
static const char *vol1[] = { "pactl", "--", "set-sink-volume", "0", "+2%", NULL };
static const char *vol2[] = { "pactl", "--", "set-sink-volume", "0", "-2%", NULL };

#include "mpdcontrol.c"

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_space,  spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_space,  spawn,          {.v = mpv } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_period, spawn,          {.v = qutebrowser } },
	{ MODKEY,                       XK_comma,  spawn,          {.v = ncmpcpp } },
	{ MODKEY|ShiftMask,             XK_period, spawn,          {.v = obs } },
	{ MODKEY|ShiftMask,             XK_comma,  spawn,          {.v = emacs } },
	{ MODKEY,                       XK_bracketleft, spawn,     {.v = vol2 } },
	{ MODKEY,                       XK_bracketright, spawn,    {.v = vol1 } },
	{ MODKEY|ShiftMask,             XK_bracketleft, mpdchange, {.i = -1} },
	{ MODKEY|ShiftMask,             XK_bracketright, mpdchange,{.i = +1} },
	{ MODKEY,                       XK_slash,  mpdcontrol,     {0} },
	{ MODKEY|ShiftMask,             XK_slash,  spawn,          {.v = pavucontrol } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_z,      togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_h,      tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_l,      tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

#endif

